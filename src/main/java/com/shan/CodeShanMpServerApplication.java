package com.shan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeShanMpServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeShanMpServerApplication.class, args);
    }

}
